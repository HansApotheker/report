import datetime
from django.shortcuts import render
from .models import Purchase


def report(request):

    objects = Purchase.objects.report(datetime.datetime(2015, 5, 10), datetime.datetime(2015, 5, 15))

    head = []
    items = {}
    for item in objects:
        if item.date_bought not in head:
            head.append(item.date_bought)

        key = (item.product_id, item.price)
        if key in items:
            items[key]['sum'][item.date_bought] = item.price*item.count
        else:
            items[key] = {'name': item.product.name,
                          'price': item.price,
                          'sum': {item.date_bought: item.price*item.count}}

    return render(request, 'report/report.html', {'head': head, 'items': items.values()})