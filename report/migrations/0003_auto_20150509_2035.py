# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('report', '0002_product_price'),
    ]

    operations = [
        migrations.RenameField(
            model_name='purchase',
            old_name='date bought',
            new_name='ts_day_start',
        ),
    ]
