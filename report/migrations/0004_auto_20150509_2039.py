# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('report', '0003_auto_20150509_2035'),
    ]

    operations = [
        migrations.RenameField(
            model_name='purchase',
            old_name='pm_id',
            new_name='pm',
        ),
    ]
