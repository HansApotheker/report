import datetime

from django.core.management.base import NoArgsCommand
from report.models import Product, Purchase
from faker import Factory
from random import randint


class Command(NoArgsCommand):
    def handle_noargs(self, **options):

        Product.objects.all().delete()
        Purchase.objects.all().delete()
        faker = Factory.create('en_US')

        product_prices = {}

        for i in range(0, 9):
            product = Product()
            product.name = faker.sentence(nb_words=2)
            product.price = 1000*randint(0, 9) + 100*randint(1, 9) + 10*randint(1, 9)
            product.save()

            prices = []
            for j in range(0, 4):
                prices.append(product.price + 100*randint(1, 9) + 10*randint(1, 9))
            product_prices[product.id] = prices

        for i in range(0, 10):
            date = datetime.datetime.now().date() - datetime.timedelta(days=i)
            products = Product.objects.all().order_by('?')[:7]

            for product in products:
                rand = randint(0, 3)
                purchase = Purchase()
                purchase.count = randint(1, 10)
                purchase.date_bought = date
                purchase.price = product_prices[product.id][rand]
                purchase.product = product
                purchase.save()