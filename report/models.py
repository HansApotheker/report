from django.db import models


class PurchaseManager(models.Manager):
    def report(self, date_start, date_end):
        from django.db import connection

        cursor = connection.cursor()
        cursor.execute("""
            SELECT ts_day_start, pm_id, price, SUM(a_count)
            FROM report_purchase
            WHERE ts_day_start >= '%s' AND ts_day_start <= '%s'
            GROUP BY ts_day_start, pm_id, price
            ORDER BY ts_day_start DESC
        """ % (date_start.strftime('%Y-%m-%d'), date_end.strftime('%Y-%m-%d')))

        # TODO: get field array directly from model
        product_ids = []
        for product in Product.objects.all():
            product_ids.append(product.id)

        result_list = []
        date_product_ids = []
        dates = []
        for row in cursor.fetchall():
            purchase = self.model(date_bought=row[0], product_id=row[1], price=row[2], count=row[3])
            result_list.append(purchase)
            date_product_ids.append(row[1])
            dates.append(row[0])

        diff = list(set(product_ids) - set(date_product_ids))
        for item in diff:
            for date in dates:
                purchase = self.model(date_bought=date, product_id=item)
                result_list.append(purchase)

        return result_list


class Product(models.Model):
    name = models.CharField(max_length=200)
    price = models.FloatField(default=0)


class Purchase(models.Model):
    date_bought = models.DateField(db_column='ts_day_start', auto_now=False)
    product = models.ForeignKey(Product, db_column='pm_id')
    price = models.FloatField(default=0)
    count = models.IntegerField(db_column='a_count', default=0)

    objects = PurchaseManager()
